<?php
	include_once '../Util/configuracao.php';

	$Acao = $_POST['Acao'];
	switch ( $Acao ) {
		case 'Executar':
			if ( $_POST['pasta'] != '' )
			{
				$pasta = str_replace( '#', "/" , $_POST['pasta'] );

				if ( strstr( $_SERVER['HTTP_USER_AGENT'], 'Linux' ) || strstr( $_SERVER['HTTP_USER_AGENT'], 'linux' ) )
				{
					$comando = "nautilus " . $pasta;
				}
				else
				{
					$pasta = str_replace( '/', "\\" , $pasta );
					$comando = "explorer.exe " . $pasta;
				}
			}

			if ( $_POST['comando'] != '' )
				$comando = trim( $_POST['comando'] );

			if ( strstr( strtolower( $_SERVER['HTTP_USER_AGENT'] ), 'linux' ) )
			{

				error_reporting( E_ALL );
				$comando = str_replace( 'abrir ', 'gedit ', $comando );
				// $retorno = shell_exec( $comando );
				$retorno = shell_exec( 'sudo gedit' );
				// $retorno = exec( $comando );

				print_r( "<pre>$retorno</pre>" );
			}
			else
			{
				$comando = trim( str_replace( '/', "\\" , $_POST['comando'] ) );
				$comando = str_replace( 'abrir ', 'notepad++ ', $comando );
				exec( $comando );
			}
			break;

		case 'Incluir':
		case 'Excluir':
			$Comando = new Conexao( CAMINHO_BANCO . "comandos.txt" );

			if ( $Acao == 'Incluir' )
				$Comando->Adicionar( $_POST['comando'] );
			elseif ( $Acao == 'Excluir' )
				$Comando->Deletar( $_POST['comando'] );
			break;

		case 'SalvarAtalho':
		case 'DeletarAtalho':
			header( 'Content-Type: application/json' );

			$Atalhos = new Conexao( CAMINHO_BANCO . "atalhos.json" );

			$titulo = explode( '/', $_POST[ 'Url' ] );
			$titulo = $titulo[ 1 ];
			$url = substr( $_POST[ 'Url' ], 1 );

			$index = NULL;
			foreach ( $Atalhos->conteudo as $key => $Atalho ) {
				if ( $Atalho->Url == $url ) {
					$index = $key;

					break;
				}
			}

			// $Funcao->deb( $Atalho );

			if ( $Acao == 'SalvarAtalho' )
			{
				$Atalho = new stdClass;
				$Atalho->Url = $url;
				$Atalho->Titulo = $titulo;

				if ( $index === NULL ) {
					$Atalhos->Adicionar( $Atalho );

					exit( json_encode( array( 'status' => true, 'Atalho' => $Atalho ) ) );
				} else {
					exit( json_encode( array( 'status' => false ) ) );
				}
			}
			elseif ( $Acao == 'DeletarAtalho' )
			{
				$deletarAtalho = $_POST[ 'Url' ] . '>>' . $titulo[ 1 ];

				if ( $index !== NULL )
					$Atalhos->Deletar( $index );

				exit( json_encode( array( 'status' => true, 'url' => $url ) ) );
			}
			break;

		default:
			exit( 'Ação não encontrada! :(' );
			break;
	}