<?php

	$diretorio = NULL;
	if ( isset( $_GET['diretorio'] ) )
		$diretorio = trim( $_GET['diretorio'] );

	if ( $diretorio )
	{
		$nomeDiretorio = explode( '/', $diretorio );

		$nomeDiretorio = $nomeDiretorio[count( $nomeDiretorio ) - 1];

		$diretorio .= "/";
	}
	else
	{
		$diretorio = "./";
		$nomeDiretorio = "Raiz (./)";
	}

	if ( is_dir( $diretorio ) )
		$sistema = opendir( $diretorio );
	else
		die( 'Diretório inválido!' );

	while ( FALSE !== ( $item = readdir( $sistema ) ) )
	{
		if ( $item == "."  || $item == ".." || ( in_array( $item, $configuracoes["ocultar"] ) && $diretorio == "./" ) || in_array( $item, $configuracoes["ignorar"] ) )
		{
			continue;
		}
		else if ( is_dir( $diretorio . $item ) )
		{
			$colecaoPastas[] = $item;
		}
		else if ( is_file( $diretorio . $item ) )
		{
			$extensao = $Funcao->extensao( $item );
			if ( $extensao == "url" )
			{
				$colecaoAtalhos[] = $item;
			}
			else
			{
				$colecaoArquivos[ $extensao ][] = $item;
				$colecaoExtensao[] = $extensao;
			}
		}
	}

	if ( !empty( $colecaoPastas ) )
		natcasesort( $colecaoPastas );

	if ( !empty( $colecaoAtalhos ) )
		natcasesort( $colecaoAtalhos );

	if ( !empty( $colecaoExtensao ) )
	{
		$colecaoExtensao = array_unique($colecaoExtensao);
		natcasesort( $colecaoExtensao );

		foreach ( $colecaoArquivos as $extensao => $colecaoFiles )
			natcasesort( $colecaoArquivos[ $extensao ] );

		natcasesort( $colecaoArquivos );
	}