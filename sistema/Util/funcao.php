<?php
	class Funcao
	{
		function extensao( $arquivo )
		{
			$posicao = strrpos( $arquivo, "." );
			
			if ( $posicao !== FALSE )
			{
				return strtolower( substr( $arquivo, $posicao + 1 ) );
			}
			else
			{
				return "";
			}
		}
		
		function tamanho( $bytes )
		{
			if ( $bytes  >= pow( 1024, 3 ) )
			{
				$bytes = round( $bytes / pow( 1024, 3 ), 0 ) . " GB";
			}
			else if ( $bytes >= pow( 1024, 2 ) )
			{
				$bytes = round( $bytes / pow( 1024, 2 ), 0 ) . " MB";
			}
			else if ( $bytes >= 1024 )
			{
				$bytes = round( $bytes / 1024, 0 ) . " KB";
			}
			else
			{
				$bytes .= " B<span style=\"color: #FFFFFF\">B</span>";
			}
			
			return $bytes;
		}
		
		function deb( $var, $die = false )
		{
			echo "<pre>";
			var_dump( $var );
			echo "</pre>";
			
			if ( $die )
				die;
		}
	}
