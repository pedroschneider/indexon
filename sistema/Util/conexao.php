<?php
	class Conexao
	{
		var $arquivo;
		var $banco;
		var $conteudo;

		function __construct( $banco )
		{
			$this->banco = $banco;
			$this->Ler();
		}

		private function Abrir( $modo )
		{
			$this->arquivo = fopen( $this->banco, $modo );
		}

		private function Ler()
		{
			$this->Abrir( 'r' );
			$this->conteudo = json_decode( fgets( $this->arquivo ) );
			$this->Fechar();

			return $this->conteudo;
		}

		private function Salvar()
		{
			$this->Abrir( 'w' );
			fwrite( $this->arquivo, json_encode( $this->conteudo ) );
			$this->Fechar();
		}

		private function Fechar()
		{
			fclose( $this->arquivo );
		}

		function Adicionar( $linha )
		{
			$this->conteudo[] = $linha;
			$this->Salvar();
		}

		function Deletar( $index )
		{
			unset( $this->conteudo[ $index ] );
			$this->Salvar();
		}
	}