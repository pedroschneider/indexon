<?php
	error_reporting( 0 );
	define( "CAMINHO_SISTEMA", "sistema/" );

	define( "CAMINHO_INC", CAMINHO_SISTEMA . "inc/" );
	define( "CAMINHO_ACAO", CAMINHO_SISTEMA . "acao/" );
	define( "CAMINHO_COMUM", CAMINHO_SISTEMA . "comum/" );
	define( "CAMINHO_JSON", CAMINHO_SISTEMA . "json/" );
	define( "CAMINHO_UTIL", CAMINHO_SISTEMA . "Util/" );

	define( "CAMINHO_JS", CAMINHO_COMUM . "js/" );
	define( "CAMINHO_CSS", CAMINHO_COMUM . "css/" );
	define( "CAMINHO_IMG", CAMINHO_COMUM . "img/" );
	define( "CAMINHO_INI", $_SERVER[ 'DOCUMENT_ROOT' ] . "/" . CAMINHO_COMUM . "ini/" );

	define( "CAMINHO_BANCO", $_SERVER[ 'DOCUMENT_ROOT' ] . "/" . CAMINHO_JSON );

	$configuracoes = parse_ini_file( CAMINHO_INI . "configuracoes.ini" , FALSE );

	$configuracoes["padrao"] = explode( ",", $configuracoes["padrao"] );
	$configuracoes["ignorar"] = explode( ",", $configuracoes["ignorar"] );
	$configuracoes["ocultar"] = explode( ",", $configuracoes["ocultar"] );
	$configuracoes["extensao"] = explode( ",", $configuracoes["extensao"] );
	$configuracoes["arquivosConf"] = explode( ",", $configuracoes["arquivosConf"] );

	include_once 'funcao.php';
	include_once 'conexao.php';

	$Funcao = new Funcao();

	header("Content-type: text/html; charset=iso-8859-1", TRUE);