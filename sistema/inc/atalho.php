<ul class="listaAtalhos clearfix">
<?php
		$Atalhos = new Conexao( CAMINHO_BANCO . "atalhos.json" );

		$colecaoAtalho = $Atalhos->conteudo;

		if ( count( $colecaoAtalho ) > 0 )
		{
			foreach ( $colecaoAtalho as $Atalho )
			{
				list( $url, $titulo ) = explode( '>>', $Atalho->Linha );
?>
	<li class="atalho" ><a href=".<?php echo $Atalho->Url; ?>" title="<?php echo $Atalho->Titulo; ?>" target="_blank" ><?php echo $Atalho->Titulo; ?></a><img src="<?php echo CAMINHO_IMG . "remover.png"; ?>" class="removerAtalho" /></li>
<?php
			}
		}
		else
		{
?>
	<li class="atalho nenhum" >Nenhum atalho encontrado</li>
<?php
		}
?>
</ul>

