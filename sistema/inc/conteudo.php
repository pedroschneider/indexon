<h2>Lista de arquivos</h2>
<?php
	if ( isset( $colecaoExtensao ) ) :
		$imgs = $configuracoes["extensao"];
		
		foreach ( $colecaoExtensao AS $Extensao ) :
?>
<br />
<h3><?php echo $Extensao; ?></h3>
<ul class="itens">
<?php
			$colecao = $colecaoArquivos[ $Extensao ];
			foreach ( $colecao AS $arquivo ) :
				$class = $Extensao;
				if ( in_array( $arquivo, $configuracoes["arquivosConf"] ) )
					$class = 'conf';
				elseif ( in_array( $arquivo, $configuracoes["padrao"] ) )
				{
					$class = 'index';

					$diretorioArquivo = $diretorio;
				}
				else
					$diretorioArquivo = $diretorio.$arquivo;
?>
	<li class="<?php echo $class; ?>" >
		<span class="nomeArquivo">
			<a href="<?php echo $diretorioArquivo; ?>" target="_blank" >
				<?php echo $arquivo; ?>
			</a>
		</span>
	</li>
<?php endforeach; ?>
</ul>
<?php
		endforeach;
	else :
?>
<ul class="itens">
	<li>Nenhum Arquivo Encontrado</li>
</ul>
<?php endif; ?>