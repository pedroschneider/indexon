var CAMINHO_ACAO;
$( document ).ready( function()
{
	CAMINHO_ACAO = $( '#hdn_caminho_acao' ).val();
	$( 'li.dir a img' ).click( function()
	{
		var CAMINHO_FISICO = $( '#hdn_caminho_fisico' ).val();
		var href = $( this ).parent().prev( 'a' ).attr( 'href' );

		href = href.split( '=./' );

		href = href[1];

		href = CAMINHO_FISICO + "#" + href;

		$.ajax( {
			url:  CAMINHO_ACAO + 'executar.php',
			dataType: 'json',
			type: 'post',
			data: { Acao: 'Executar', pasta: href }
		} );
	} );

	$( 'li.comando span' ).click( function()
	{
		var comando = $( this ).text();

		$.ajax( {
			url:  CAMINHO_ACAO + 'executar.php',
			dataType: 'json',
			type: 'post',
			data: { Acao: 'Executar', comando: comando }
		} );

	} );

	$( '#txt_comando' ).blur( function()
	{
		var comando = $( this ).val();

		if ( comando != '' )
		{
			$.ajax( {
				url:  CAMINHO_ACAO + 'executar.php',
				dataType: 'json',
				type: 'post',
				data: { Acao: 'Incluir', comando: comando },
				success: function()
				{
					$( 'ul.listaComando' ).append( '<li class="comando" ><span>' + comando + '</span><input type="button" value="-" id="bt_menos" /></li>' );
					aciona_botao_excluir();
					aciona_executar();
				}
			} );
		}

		$( this ).val( 'Adicionar' );
	} ).focus( function()
	{
		$( this ).val( '' );
	} );

	aciona_botao_excluir();
	aciona_executar();
	salva_atalho();
	deletar_atalho();

	$( 'img.programa' ).click( function()
	{
		var comando = $( this ).next( '#hdn_comando' ).val();

		$.ajax( {
			url:  CAMINHO_ACAO + 'executar.php',
			dataType: 'json',
			type: 'post',
			data: { Acao: 'Executar', comando: comando }
		} );
	} );

} );

function salva_atalho()
{
	$( 'li.index span a' ).live( 'click', function ()
	{
		var url = $( this ).attr( 'href' );
		$.post( CAMINHO_ACAO + 'executar.php', { Acao: 'SalvarAtalho', Url: url }, function ( data ) {
			if ( data.status == true ) {
				console.log( data.status )
				$( '.listaAtalhos' ).find( '.nenhum' ).remove();
				$( '.listaAtalhos' ).append( '<li class="atalho"><a target="_blank" title="' + data.Atalho.Titulo + '" href=".' + data.Atalho.Url + '">' + data.Atalho.Titulo + '</a><img class="removerAtalho" src="sistema/comum/img/remover.png"></li>' );
			};
		} );
	} );
}

function deletar_atalho()
{
	$( '.listaAtalhos .atalho img' ).live( 'click', function ()
	{
		var atalho =  $( this ).parent( '.atalho' );
		var url = atalho.find( 'a' ).attr( 'href' );
		$.post( CAMINHO_ACAO + 'executar.php', { Acao: 'DeletarAtalho', Url: url }, function () {
			atalho.remove();
			nenhum_atalho();
		} );

		return false;
	} );
}

function nenhum_atalho () {
	var ulAtalhos = $( '.listaAtalhos' );
	if ( ulAtalhos.find( 'li' ).length == 0 )
		ulAtalhos.html( '<li class="atalho nenhum" >Nenhum atalho encontrado</li>' );
}

function aciona_executar()
{
	var CAMINHO_ACAO = $( '#hdn_caminho_acao' ).val();

	$( 'li.comando span' ).click( function()
	{
		var comando = $( this ).text();

		$.ajax( {
			url:  CAMINHO_ACAO + 'executar.php',
			dataType: 'json',
			type: 'post',
			data: { Acao: 'Executar', comando: comando }
		} );

	} );
}
function aciona_botao_excluir()
{
	var CAMINHO_ACAO = $( '#hdn_caminho_acao' ).val();

	$( 'li.comando #bt_menos' ).click( function()
	{
		var button = $( this );
		var comando = button.parent().find( 'span:first' ).text();

		$.ajax( {
			url:  CAMINHO_ACAO + 'executar.php',
			dataType: 'json',
			type: 'post',
			data: { Acao: 'Excluir', comando: comando },
			success: function()
			{
				button.parent( 'li' ).remove();
			}
		} );
	} );
}