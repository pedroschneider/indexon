<?php
	include_once 'sistema/Util/configuracao.php';
	
	include_once CAMINHO_ACAO . 'home.php';

	include_once CAMINHO_INC . 'cabecalho.php';
?>
<body id="duascolunas" class="duascolunas-a">
<input type="hidden" value="<?php echo CAMINHO_ACAO; ?>" id="hdn_caminho_acao" />
<input type="hidden" value="<?php echo str_replace( '\\', '#', dirname(__FILE__) ); ?>" id="hdn_caminho_fisico" />
	<div id="tudo">
	
		<div id="topo">
			<?php include_once CAMINHO_INC . 'topo.php'; ?>
		</div>
		
		<div id="atalhos">
			<?php include_once CAMINHO_INC . 'atalho.php'; ?>
		</div>
		
		<div id="nav">
			<?php include_once CAMINHO_INC . 'menu.php'; ?>
		</div>
		
		<div id="principal">
			<?php include_once CAMINHO_INC . 'conteudo.php'; ?>
		</div>
		
		<div id="apoio">
			<?php include_once CAMINHO_INC . 'apoio.php'; ?>
		</div>
		
		<div id="rodape">
			<?php include_once CAMINHO_INC . 'rodape.php'; ?>
		</div>
	
	</div>
</body>
